import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ListView,
  FlatList,
  Button
} from 'react-native';
import { WebBrowser } from 'expo';
import AwesomeButton from 'react-native-really-awesome-button'
import AwesomeButtonRick from 'react-native-really-awesome-button/src/themes/rick';
import { MonoText } from '../components/StyledText';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props);
    this.state = ({
      data:[],
      info:{
        "name": "Bitcoin", 
        "quotes": {
          "TRY": {
              "price": 0, 
              "volume_24h": 2956835920.4260383, 
              "market_cap": 9473035235.0, 
              "percent_change_1h": 0.42, 
              "percent_change_24h": 4.41, 
              "percent_change_7d": -17.69
          }, 
          "USD": {
              "price": 30.3919884344, 
              "volume_24h": 562727398.24261, 
              "market_cap": 1802851634.0, 
              "percent_change_1h": 0.42, 
              "percent_change_24h": 4.41, 
              "percent_change_7d": -17.69
          }
      },
      }
    });
    this.getPrices();

    this.onPressLearnMore = this.onPressLearnMore.bind(this);
  }

  getInfo(item){
    let url = 'https://api.coinmarketcap.com/v2/ticker/' + item.id +'/?convert=TRY';

    fetch(url).then((data) => data.json()).then((data) => {
      this.setState({
        info:data.data
      });
      console.log(data);
    })
  }

  getPrices(){
    let url = 'https://api.coinmarketcap.com/v2/listings/';

    fetch(url).then((data) => data.json()).then((data) => {
      this.setState({
        data:data.data
      });
      console.log(data);
    })
  }

  onPressLearnMore(item){
      this.getInfo(item);
  }

  render() {
    
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <FlatList
            data={this.state.data}
            renderItem={({item}) => 
                <AwesomeButtonRick type="primary"
                style={styles.btnKripto}
                onPress={()=>{
                  //this.onPressLearnMore.bind(this);
                  this.onPressLearnMore(item);
                }}
               
              >
                {item.name}
              </AwesomeButtonRick>
            }
          />
        </ScrollView>

        <View style={styles.tabBarInfoContainer}>
          
            <Text style={styles.tabBarInfoText}>{this.state.info.name}in Türk Lirası Değeri</Text>
          

          <View style={[styles.codeHighlightContainer, styles.navigationFilename]}>
            <MonoText style={styles.codeHighlightText}>{this.state.info.quotes.TRY.price}</MonoText>
          </View>
        </View>
      </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  btnKripto:{
    
    
    marginTop:15
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
    alignItems: 'center',
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
